import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    speed: {
      type: Number,
      value: 2
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    componentWidth: '',
    componentHeight: '',
    step: '',
    left: '0',
    contentList: [],
    totalWidth: '100000'
  },
  ready: function() {
    const that = this;
    
  },

  /**
   * 组件的方法列表
   */
  methods: {
    init: function() {
      const that = this;
      let query = wx.createSelectorQuery().in(this)
      let contentList = that.data.contentList;
      query.select('#marquee').boundingClientRect(function(rect) {
        contentList.map(function(item, index, input) {
          item.lineHeight = rect.height;
          return item;
        })

        that.setData({
          step: rect.width / (that.data.speed * 20),
          // contentList: contentList
        })
      }).exec();
      query.selectAll('.marquess-item').boundingClientRect(function(res) {
        let totalWidth = 0;
        for (let i = 0; i < res.length; i++) {
          contentList[i].left = totalWidth;
          totalWidth += res[i].width;
          contentList[i].width = res[i].width;
          contentList[i].position = 'absolute';
        }
        // console.log(totalWidth + res[0].width)
        that.setData({
          totalWidth: totalWidth,
          contentList: contentList
        })
      }).exec()
      setTimeout(() => {
        that.marqueeRun();
      }, 1000)

    },
    marqueeRun: function() {
      const that = this;
      const contentList = that.data.contentList;
      const contentFirst = contentList[0];
      let contentLast = new Object();
      contentLast.id = contentFirst.id;
      contentLast.title = contentFirst.title;
      contentLast.lineHeight = contentFirst.lineHeight;
      contentLast.position = contentFirst.position;
      contentLast.left = that.data.totalWidth;
      contentLast.width = contentFirst.width;  
      contentList[contentList.length] = contentLast;
      that.setData({
        contentList: contentList,
        totalWidth: that.data.totalWidth + contentLast.width
      })    
      setInterval(() => {
        let left = parseInt(that.data.left) - parseFloat(that.data.step);
        if (left < -1 * that.data.totalWidth) {
          left = 0
        }
        that.setData({
          left: left
        })
      }, 50)
    }
  }
})