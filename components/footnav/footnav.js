// components/footnav/footnav.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title: String,//按钮文字，必填
    opentype:{  //按钮button的open-type属性，可选
      type: String,
      value:''
    },
    page:{  //按钮跳转页面路径
      type: String,
      value:''
    },
    navigate: { //跳转页面是否 tabBar 页面
      type: Boolean,
      value:true
    }

  },

  /**
   * 组件的初始数据
   */
  data: {
    flag:'share',
  },

  lifetimes: {
    attached: function () {
      console.log(344)
      if(!wx.getStorageSync('user')){
        this.setData({
          flag:''
        })
      }
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行
    },
  },

  /**
   * 组件的方法列表
   */
  methods: {
    topage:function(){
      const that=this;
      if(that.data.page){
        if(that.data.navigate){
            wx.navigateTo({
              url: that.data.page,
            })
        }else{
            wx.switchTab({
              url: that.data.page,
            })
        }
      }
    },
    toIndex:function(){
      wx.switchTab({
        url: '/pages/index/index',
      })
    },
    showtoast:function(){
      if(this.data.flag==''){
        wx.showToast({
          title: '您暂未登录，请先登录！',
          icon: 'none'
        })
      }
    }
  }
})
