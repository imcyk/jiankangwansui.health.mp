// components/switchtab.js
import Honey from '../utils/honey';

const app = getApp();
const honey = new Honey();
import util from '../utils/util.js'

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    myProperty: { // 属性名
      type: String,
      value: 0
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    imgurl:'',
    centerPicImageViewSuffix: util.imageViewToWidthFunc(90),
  },

  /**
   * 组件的方法列表
   */
  methods: {
      toMine:function(e){  
      const formId = e.detail.formId
      if(wx.getStorageSync('user')){
        honey.post({
          alias: 'saveFormId',
          data: {
            form_id: formId
          },
          success: function (res) { }
        })
      }
       wx.switchTab({
         url: '/pages/mine/newindex',
       })
      },
    toKnow:function(e){
      if(wx.getStorageSync('user')){
        wx.navigateTo({
          url: '/pages/mine/mypromote',
        })
      }else{
        wx.showToast({
          title: '您暂未登录，请先登录!',
          icon:'none'
        })
      }
       
    }
  },

  lifetimes: {
    attached: function () {
      const that=this;
      // 在组件实例进入页面节点树时执行
      honey.post({
        alias:'getPagesConfig',
        success(res){
          that.setData({
            imgurl: res.data.data.pagesConfig.index.mid_button
          })
          
        }

      })
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行
    },
  },
})
