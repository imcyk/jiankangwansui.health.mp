import md5 from "./md5.js"
const app = getApp();
const config = {
  mainColor: '#259b24',
  key: 'JKWS2019ANYIDC',
  header: () => {
    const version = 1.4;
    //这里可以写各种方法；如getToken()，但记得必须return;
    const user = wx.getStorageSync('user')
    let token = '';
    if (user) {
      token = user.token;
    }
    const headerParams = {
      did: md5(Date.parse(new Date().toString()).toString()+Math.random() * 1000),
      noncestr: md5(Date.parse(new Date().toString()).toString() + Math.random()*100),
      token:token,
      // token:'',
      type: 'min',
      version: version,
    }
    // const headerParamsStr='';
    const headerParamsArray = [];
    for (const param in headerParams) {
      headerParamsArray.push(`${param}=${headerParams[param]}`)
    }

    // console.log(md5(md5(headerParamsArray.join('&')) + config.key))
    const sign = md5(md5(headerParamsArray.join('&')) + config.key);


    return {
      'content-type': 'application/x-www-form-urlencoded',
      did: headerParams.did,
      noncestr: headerParams.noncestr,
      token: headerParams.token,
      type: headerParams.type,
      version: headerParams.version,
      sign,

      //key:value Or key:getValue();
    }
  },
  // apiUrl: 'http://192.168.2.110/yiliao/phalapi/public/',
  // apiUrl: 'http://192.168.2.201/long_live_health/phalapi/public/',

  // apiUrl: 'https://xmjkws.com/phalapi/public/',
  // apiPrefix: '?service=',
  // apiUrl: 'https://xy2019.5fool.com/',
  apiUrl: 'https://new.xmjkws.com/',
  apiPrefix: '',
  apiMap: {
    //alias:'value'
    myCommentList:'api/v1/myCommentList',//我的评价
    userShare:'api/v1/userShare',//推广码
    getShareInfo:'api/v1/getShareInfo',//分享信息
    getCouponList: 'api/v1/getCouponList',//兑换券接口
    getSearchHot:'api/v1/getSearchHot',//获取搜索热词
    buySuperGift:'api/v1/buySuperGift',//购买超级福利接口
    superGift:'api/v1/superGift',//超级福利接口
    getPagesConfig:'api/v1/getPagesConfig',//首页底部圆形图标
    updateProfile: 'api/v1/updateProfile',//修改用户信息接口
    getNotice:'api/v1/getNotice',//跑马灯公告详情接口
    saveFormId: 'api/v1/saveFormId',//小程序formid接口
    helpReport:'api/v1/helpReport',//帮查报告接口
    checkSms:'api/v1/checkSms',//检测验证码
    sendSms:'api/v1/sendSms',//发送短信
    doWithdraw:'api/v1/doWithdraw',//提现接口
    uploadImg:'api/v1/uploadImg',//图片上传接口
    uploadVideo:'api/v1/uploadVideo',//视频上传接口
    infoDatement:'api/v1/infoDatement',//预约详情接口
    cancelOrder: 'api/v1/cancelOrder', //取消订单接口
    payOrderData: 'api/v1/payOrderData', //我的订单付款数据接口
    payOrder: 'api/v1/payOrder', //我的订单付款接口
    datementData: 'api/v1/datementData', //预约数据接口
    createOrder: 'api/v1/createOrder', //添加订单
    upgradePage: 'api/v1/upgradePage', //购买等级富文本
    toUpgrade: 'api/v1/toUpgrade', //推广/董事升级接口和购买等级接口
    unbindUserPhone: 'api/v1/unbindUserPhone', //手机号解绑
    bindUserPhone: 'api/v1/bindUserPhone', //微信手机号绑定
    minSessionKey: 'api/v1/minSessionKey', //微信sessionKey获取
    minLogin: 'api/v1/minLogin', //微信授权登录
    broadcast: 'App.User.Broadcast',
    // register: 'App.User.Register',//旧注册接口
    // sendCode: 'App.User.Send_code',//旧短信接口
    // login: 'App.User.Login',//旧登录接口
    // bulletin: 'App.User.Bulletin',
    articleList: 'App.User.Article_list',
    // comboList: 'App.User.Car_list',//商品列表接口
    getGoodsList: 'api/v1/getGoodsList', //商品列表接口
    categoryList: 'App.User.Category_list', //商品分类接口
    getShopCate: 'api/v1/getShopCate', //商品分类接口
    // comboDetail: 'App.User.Car_info',//商品详情接口
    getGoodsInfo: 'api/v1/getGoodsInfo', //商品详情接口
    // chatList: 'App.User.Chat_list',//聊天列表接口，废弃
    // addChat: 'App.User.Add_chat',添加聊天记录接口，废弃
    // storeList: 'App.User.Store_list',//机构列表接口
    storeList: 'api/v1/storeList', //机构列表接口
    // storeInfo: 'App.User.Store_info',//机构详情
    storeInfo: 'api/v1/storeInfo', //机构详情
    // referee: 'api/v1/myReferee',//旧推荐人接口
    myReferee: 'api/v1/myReferee', //推荐人接口
    // addReferee: 'api/v1/bindReferee',//旧设置推荐人接口
    bindReferee: 'api/v1/bindReferee', //设置推荐人接口
    // orderList: 'App.User.Order_list',//旧我的订单接口
    orderList: 'api/v1/orderList', //我的订单接口
    // renewLog: 'App.User.Renew_log',//旧明细接口
    amountLog: 'api/v1/amountLog', //明细接口
    commentList: 'App.User.Comment_list',
    // myComment: 'App.User.My_comment',//旧评价列表
    commentList:'api/v1/commentList',//评论列表接口
    addFamily: 'App.User.Add_family',
    myAppointmentUser: 'api/v1/myAppointmentUser', //我的预约人信息
    familyInfo: 'App.User.Family_info',
    updAppointmentUser: 'api/v1/updAppointmentUser', //添加更新预约人信息
    updateFamily: 'App.User.Upd_family',
    // delFamily: 'App.User.Del_family',//旧删除预约人
    delAppointmentUser: 'api/v1/delAppointmentUser', //删除预约人
    // myBespeak: 'App.User.My_bespeak',//旧我的预约接口
    myDatement: 'api/v1/myDatement', //我的预约接口
    // updateHeadImage: 'App.User.Upd_headimg',//旧上传修改头像接口
    getUserInfo: 'api/v1/getMinUser',
    // updateNickName: 'App.User.Upd_nike_name',//旧修改昵称接口
    forgetPassword: 'App.User.Forget_password',
    // fenxiaoOrder: 'App.User.Fenxiao_order',//旧团队订单
    fenxiaoOrder: 'api/v1/fenxiaoOrder', //旧团队订单
    // myXiaji: 'App.User.My_xiaji',//旧我的伙伴列表
    getChildList: 'api/v1/getChildList', //我的伙伴列表
    getChildInfo: 'api/v1/getChildInfo', //伙伴，大使详情
    // withdraw: 'App.User.Withdraw',//旧提现接口
    // dongshiOrder: 'App.User.Dongshi_order',//旧运营订单
    dongshiOrder: 'api/v1/dongshiOrder', //运营订单
    bulletinInfo: 'App.User.Bulletin_info',//旧跑马灯详情接口
    // richText: 'App.User.Rich_text',//旧富文本接口
    userAmount: 'api/v1/userAmount', //各基金汇总及说明界面接口
    // insertOrder: 'App.User.Insert_order',//旧添加订单
    orderInfo: 'api/v1/orderInfo',//订单详情接口
    // addBespeak: 'App.User.Add_bespeak',//旧添加预约接口
    makeDatement: 'api/v1/makeDatement', //旧添加预约接口
    // addComment: 'App.User.Add_comment',//旧提交评论接口
    commentOrder: 'api/v1/commentOrder',//提交评论接口
    // letterList: 'App.User.Letter_list',//旧站内信接口
    userLetter: 'api/v1/userLetter', //站内信接口
    // quanyi: 'App.User.Quanyi',//旧权益接口
    userEquity: 'api/v1/userEquity', //权益接口
    // upPay: 'App.User.Up_pay',//旧自费升级接口
    updateGrade: 'api/v1/updateGrade', //旧自费升级接口
    // rechargeSet: 'App.User.Recharge_set',//旧充值接口
    recharge: 'api/v1/recharge', //充值接口
    // getTese: 'App.User.Get_tese',
    // actionInfo: 'App.User.Action_info',//旧活动详情
    activityInfo: 'api/v1/activityInfo', //旧活动详情
    // wxLogin: 'App.User.Wx_login',//登陆接口废弃
    // report: 'App.User.Report',//旧查询报告接口
    userReport: 'api/v1/userReport', //旧查询报告接口
    // getNewChat: 'App.User.Get_new_chat',//获取新聊天信息接口废弃
    // healthArticleList: "App.Health.ArticleList",//旧文章列表接口
    getArticleList: "api/v1/getArticleList", //文章列表接口
    // healthArticleInfo: "App.Health.ArticleInfo",//旧文章详情接口
    getArticleInfo: "api/v1/getArticleInfo", //文章详情接口
    // healthArticleFund: "App.Health.ArticleFund",//旧阅读健康知识领红包接口
    readGiftHealth: "api/v1/readGiftHealth", //阅读健康知识领红包接口
    homeHomeData: "api/v1/getHomeData",//获取首页数据接口
    // homeCommonProblem: "App.Home.CommonProblem",//旧常见问题接口
    clientQuestion: "api/v1/clientQuestion", //常见问题接口
    // myXiajiInfo: "App.User.My_xiaji_info",//旧伙伴大使详情
    // SmsSwitch: "App.User.Sms_switch",//旧短信开关接口
    // GetStoreCar: "App.User.Get_store_car",//废弃机构预约页
    // CommonProblemInfo: "App.Home.CommonProblemInfo",//旧常见问题详情接口
    questionDetail: "api/v1/questionDetail", //常见问题详情接口
    // GetUserQrcode: 'App.Home.GetUserQrcode'//旧小程序码接口
    userQrcode: 'api/v1/userQrcode' //小程序码接口
  },
  //最终的请求地址为apiUrl + apiPrefix + apiMap
  regular: {
    phone: {
      exp: /^1\d{10}$/,
      text: '手机号码不正确\n请确认后输入'
    },
    code: {
      exp: /^\d{6}$/,
      text: '请输入正确的验证码'
    },
    password: {
      exp: /^[a-zA-Z0-9]{6,20}$/,
      text: '密码由6～20位英文大小写或数字组合'
    }
  },
  checkLogin: {
    keyUser: 'user', //用户信息缓存
    keyReferrer: 'referrer', //来源页缓存
    defaultReferrer: '/pages/index/index', //默认来源页
    loginPath: '/pages/index/index',
    isTab: true, //默认来源页是否为tab页面
  }
};

module.exports = config;  //CommonJS写法
// export default config  //es6写法