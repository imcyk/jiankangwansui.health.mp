import Honey from '/utils/honey';
import Event from '/utils/event'


const honey = new Honey();
const event = new Event();


App({
  onLaunch: function(e) {
    if (!wx.getStorageSync('user')){
      let query = [];
      for (var key in e.query) {
        query.push(key + '=' + e.query[key])
      }
      query = query.join('&') 
      wx.setStorageSync('pagesUrl', '/'+e.path+'?'+query)
    }
    
    
    const that = this;
    try {
      const systemInfo = wx.getSystemInfoSync();
      // console.log(systemInfo.system) 
      if (systemInfo.system.indexOf('iOS') >= 0) {
        that.globalData.system = 'ios';
      } else {
        that.globalData.system = 'android';
      }
    } catch (e) {
      that.globalData.system = 'android'
    }

    wx.hideTabBar({})
    const userStorage = wx.getStorageSync('user');
    const phoneStorage = wx.getStorageSync('phone');
    const passwordStorage = wx.getStorageSync('password');
    // if (userStorage != '') {
    //   if (phoneStorage != '' && passwordStorage != '') {
    //     that.refreshUserInfo();
    //   } else {
    //     wx.removeStorageSync('user')
    //   }
    // }
    that.refreshUserInfo();
    event.on('refreshUserInfo', 'startUp', function(data) {
      that.refreshUserInfo();
    })

    // 登录
    wx.login({
      success: res => {
        honey.post({
          alias: 'minSessionKey',
          data: {
            code: res.code,
          },
          success: function(res) {
            wx.setStorageSync('sessionKey', res.data.data.result.session_key)
          }
        })
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  onShow: function() {
    const updateManager = wx.getUpdateManager();
    updateManager.onUpdateReady(function() {
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success: function(res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })
  },
  toShow: function (e) {
    const that = this;
    const type = e.currentTarget.dataset.type;
    const id = e.currentTarget.dataset.id;
    if (type == "8") {
      wx.navigateTo({
        url: '/pages/article/lunbo?id=' + id,
      })
    }
    if (type == "2") {
      wx.navigateTo({
        url: '/pages/combo/newdetail?id=' + id,
      })
    }
    if (type == "7") {
      wx.navigateTo({
        url: '/pages/organization/newindex?id=' + id,
      })
    }
    if (type == "9") {
      wx.navigateTo({
        url: '/pages/mine/superWelfare?id=' + id,
      })
    }
    if (type == "4") {
      wx.navigateTo({
        url: id,
      })
    }
  },
  globalData: {
    userInfo: null,
    system: 'android'
  },
  refreshUserInfo: function() {
    const that = this;
    let user = wx.getStorageSync('user');
    if (user.id && user.token) {
      honey.post({
        alias: 'getUserInfo',
        data: {
          user_id: user.id,
          token: user.token
        },
        success: function(res) {
          user = Object.assign(user, res.data.data.userInfo);
          wx.setStorageSync('user', user);
          event.emit('getUserInfo', user);
        }
      })
    }
  }
})