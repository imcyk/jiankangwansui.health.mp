import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user:"",
    longitude: '',
    latitude: '',
    scale: '',
    markers: '',
    polyline: '',
    includeOpoints: '',
    organizationList: [],
    markers: [],
    srollToStoreId: '',
    searchName: '',
    daxiao:'点击缩放',
    mapactive:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code)
        wx.setStorageSync('pagesUrl', '/pages/location/index'),
        wx.setStorageSync('isTab', true),
      wx.switchTab({
        url: '/pages/index/index'
      })
    }
    wx.hideTabBar({})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    wx.getLocation({
      type: 'gcj02',
      complete: function(res) {
        that.setData({
          longitude: res.longitude ? res.longitude : 118.111986,
          latitude: res.latitude ? res.latitude : 24.47489,
          user:wx.getStorageSync('user')
        })
        that.getStoreList()
      },
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/location/index?check_code=${that.data.user.check_code}`
    }
  },
  getStoreList: function() {
    const that = this;
    honey.post({
      alias: 'storeList',
      data: {
        page_no: 1,
        page_size: 5000,
        keyword: that.data.searchName,
        lng: that.data.longitude,
        lat: that.data.latitude
      },
      success: function(res) {
        const organizationList = res.data.data.storeList.list;
        that.setData({
          organizationList,
          markers: that.genMarkers(organizationList)
        })
      }
    })
  },
  genMarkers: function(list) {
    let markerList = [];
    for (let i = 0; i < list.length; i++) {
      let marker = new Object();
      const store = list[i];
      marker.id = store.store_id;
      marker.latitude = store.lat;
      marker.longitude = store.lng;
      marker.label = {
        content: store.store_name
      };
      markerList.push(marker);
    }
    return markerList;
  },
  tapMarker: function(e) {
    const that = this;
    that.setData({
      srollToStoreId: `store${e.markerId}`
    })
  },
  inputName: function(e) {
    const that = this;
    that.setData({
      searchName: e.detail.value
    })
  },
  searchOrganization: function() {
    const that = this;
    that.getStoreList();
  },
  daxiao:function(){
    const that = this;
    if (that.data.mapactive==false){
    that.setData({
      mapactive:true,
    })
    }else{
      that.setData({
        mapactive: false,
      })
    }
  },
  toChangeBtn: function () {
    const that = this;
    that.setData({
      searchFocus: true
    })
  },
  toRestoreBtn: function () {
    const that = this;
    that.setData({
      searchFocus: false
    })
  }
})