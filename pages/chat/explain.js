import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headImage: '',
    text:'',
    rulesShow: false,
    healthstatus: false,
    lists:{},
    cateList:{},
    cate_id:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this;
    that.homeCommonProblem();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  homeCommonProblem:function(){
    const that = this;
    honey.post({
      alias: 'clientQuestion',
      data: {
        page_no: 1,
        page_size: 10000,
        cate: that.data.cate_id,
      },
      success: function (res) {
        that.setData({
          lists: res.data.data.articleList,
          cate_id:that.data.cate_id,
          cateList: res.data.data.cateList,
          headImage: res.data.data.topBanner[0],
          phoneNumber: res.data.data.client_tel
        })
        if (that.data.cate_id==''){
          that.setData({
            cate_id: res.data.data.cateList[0]
          })
        }
      }
    })
  },
  cateClick:function(e){
    const that = this;
    that.setData({
      cate_id: e.currentTarget.dataset.cateId,
      page_no: 1
    })
    that.homeCommonProblem();
  },
  navitorTo:function(e){
    console.log(e);
    const that=this;
    const value = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/mine/kefuqa?id='+value,
    })
  },
  tel:function(){
    const that = this;
    const phoneNumber = that.data.phoneNumber;
    wx.makePhoneCall({
      phoneNumber,
    })
  }
})