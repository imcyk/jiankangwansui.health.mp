import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageNo: 1,
    pageSize: 10,
    comboList: [],
    canLoad: true,
    user: {},
    keyword: '',
    searchFocus: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    const keyword = options.keyword;
    if (keyword) {
      that.setData({
        keyword: keyword
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    that.getComboList();
    const user = honey.checkLogin('/pages/search/index', false);
    that.setData({
      user: user
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  getComboList: function() {
    const that = this;
    if (that.data.canLoad) {
      that.setData({
        canLoad: false
      })
      honey.post({
        alias: 'getGoodsList',
        data: {
          page: that.data.pageNo,
          page_size: that.data.pageSize,
          keyword: that.data.keyword,
          sort:1
        },
        success: function(res) {
          that.setData({
            comboList: honey.mergeArray(that.data.comboList, res.data.data.goodsList),
            pageNo: that.data.pageNo + 1,
            canLoad: (res.data.data.length < that.data.pageSize ? false : true)
          })
        },
        fail: function() {
          that.setData({
            canLoad: true
          })
        }
      })
    }
  },
  tapCategoty: function(e) {
    const that = this;
    const catId = e.currentTarget.dataset.catId;
    that.setData({
      canLoad: true,
      pageNo: 1,
      catId: catId,
      comboList: []
    })
    that.getComboList()
  },
  inputKeyword: function(e) {
    const that = this;
    that.setData({
      keyword: e.detail.value
    })
  },
  toSearch: function() {
    const that = this;
    that.setData({
      pageNo: 1,
      canLoad: true,
      comboList: []
    })
    that.getComboList();
  },
  toChangeBtn: function () {
    const that = this;
    that.setData({
      searchFocus: true
    })
  },
  toRestoreBtn: function () {
    const that = this;
    that.setData({
      searchFocus: false
    })
  }
})