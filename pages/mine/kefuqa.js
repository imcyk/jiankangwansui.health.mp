// pages/mine/healthdetail.js
import Honey from '../../utils/honey';
import Event from '../../utils/event'


const app = getApp();
const honey = new Honey();
const event = new Event();


Page({

  /**
   * 页面的初始数据
   */
  data: {
    data: "",
    content: '',
    healthId: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this;
    const id = options.id
    that.setData({
      healthId: id
    })
    honey.post({
      alias: 'questionDetail',
      data: {
        id: that.data.healthId,  
      },
      success: function (res) {
        console.log(res.data.data)
        const handleBr = new RegExp('\r\n', "g");
        that.setData({
          data: res.data.data,

          content: res.data.data.content.replace(handleBr, '<br>').replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin:0 auto;" ')
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
})