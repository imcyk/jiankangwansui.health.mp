import Honey from '../../utils/honey';
import Event from '../../utils/event'


const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {},
    nickName: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myAccount', false);
    that.setData({
      user: user
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  inputNickName: function(e) {
    const that = this;
    that.setData({
      nickName: e.detail.value
    })
  },
  saveNickName: function() {
    const that = this;
    const user = that.data.user;
    const nickName = that.data.nickName.trim();
    if (nickName != '') {
      honey.post({
        alias: 'updateProfile',
        data: {
          nickname: nickName,
          sms_enable: that.data.user.sms_enable
        },
        success: function(res) {
          event.emit('refreshUserInfo', 'userInfo');
          honey.showMiniToast({
            title: '用户名修改成功',
          })
          setTimeout(() => {
            wx.navigateBack();
          }, 1000)
        }
      })
    }
  }
})