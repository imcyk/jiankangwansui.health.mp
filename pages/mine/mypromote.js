// pages/mine/mypromote.js
import Honey from '../../utils/honey';
import Event from '../../utils/event'


const app = getApp();
const honey = new Honey();
const event = new Event();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    myPromoteImage: "",
    user: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    if (wx.getStorageSync("user")) {
      const user = wx.getStorageSync("user");
      that.setData({
        user: user,
        // myPromoteImage: user.share_img + '?_t=' + Date.parse(new Date())
      })
    } else {
      honey.checkLogin('/pages/mine/mypromote', true)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.userShare()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
      return {
        title: `平台烧钱啦～点击领取大红包！`,
        path: `/pages/index/index?check_code=${that.data.user.check_code}`,
        imageUrl: '/res/activity1.jpeg'
      }   
  },
  saveImage: function() {
    const that = this;
    wx.getImageInfo({
      src: that.data.user.share_img,
      success: function(ret) {
        var path = ret.path;
        wx.saveImageToPhotosAlbum({
          filePath: path,
          success(result) {
            wx.showToast({
              title: '保存成功',
            })
          }
        })
      }
    })
  },
  saveFriend: function() {
    const that = this;
    that.onShareAppMessage();
  },

  //推广码
  userShare:function(){
    wx.showLoading({
      title: '加载中...',
    })
    const that=this;
    honey.post({
      alias: 'userShare',
      success: function (res) {
         that.setData({
           myPromoteImage: res.data.data.share_img
         }
         )
      },
      complete:function(){
        wx.hideLoading();
      }
      
    })
    
  },

  openimage:function(){
    wx.previewImage({
      current: this.data.myPromoteImage, // 当前显示图片的http链接
      urls: [this.data.myPromoteImage] // 需要预览的图片http链接列表
    })
  }

})