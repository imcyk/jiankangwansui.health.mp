import Honey from '../../utils/honey';
import Event from '../../utils/event'


const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {},
    backageImage: wx.getStorageSync('backageImage')
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    const res = wx.getSystemInfoSync();
    const windowWidth = res.windowWidth;
    const canvasWidth = windowWidth;
    const canvasHeight = canvasWidth * 1.63148;

    that.setData({
      canvasWidth: canvasWidth,
      canvasHeight: canvasHeight
    })

    honey.post({
      alias: 'articleList',
      data: {
        type: 7,
        article_id: 13
      },
      success: function(res) {
        const backageImage = res.data.data[0].img;
        // if(that.this)
        that.setData({
          backageImage: res.data.data[0].img
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myQrcode', false);
    that.setData({
      user: user
    })

    wx.showLoading({
      title: '正在生成邀请码',
      mask: true
    })
    const ctx = wx.createCanvasContext('qrcodeCanvas');
    https: //xmjkws.com/aaa/bbb.jpg
      wx.downloadFile({
        url: 'https://xmjkws.com/aaa/bbb.jpg',
        success: function(bg) {
          if (bg.statusCode === 200) {
            ctx.drawImage(bg.tempFilePath, 0, 0, that.data.canvasWidth, that.data.canvasHeight);
            wx.downloadFile({
              url: 'https://xmjkws.com/index.php/Qrcode/get_qrcode?user_id=' + user.id,
              success: function(res) {
                // 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容
                if (res.statusCode === 200) {

                  ctx.drawImage(res.tempFilePath, that.data.canvasWidth * 0.32, that.data.canvasHeight * 0.42237, that.data.canvasWidth * 0.3707, that.data.canvasWidth * 0.3707);
                  ctx.draw();
                  wx.hideLoading()
                  wx.showToast({
                    title: '二维码已生成\n长按可下载二维码',
                    icon: 'none'
                  })
                }
              }
            })
          }
        }
      })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  saveQrcode: function() {
    const that = this;
    const ctx = wx.createCanvasContext('qrcodeCanvas');

    wx.canvasToTempFilePath({
      canvasId: 'qrcodeCanvas',
      success: function(res) {
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success(saveRes) {
            wx.showToast({
              title: '保存成功',
            })
          }
        })
      }
    })
  }
})