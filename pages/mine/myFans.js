import Honey from '../../utils/honey';
import Event from '../../utils/event'

const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: '',
    pageNo: 1,
    pageSize: 10,
    isLoad: true,
    fansList: [],
    fansSwitch:'first',
    jibie:'first',
    team:{},//一二级伙伴，我的大使数量
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', '/pages/mine/myFans'),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    const that = this;
    event.on('getUserInfo', 'myAccount', function (data) {
      that.setData({
        user: data
      })
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myAmbassadorOrder', true);
    that.setData({
      user: user
    })
    that.getFansList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    const that = this;
    that.getFansList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mine/myFans?check_code=${that.data.user.check_code}`,
    }
  },
  getFansList: function() {
    const that = this;
    if (that.data.isLoad) {
      that.setData({
        isLoad: false
      })
      honey.post({
        alias: 'getChildList',
        data: {
          page: that.data.pageNo,
          page_size: that.data.pageSize,
          cate:'team',
          type: that.data.jibie
        },
        success: function(res) {
          that.setData({
            team:res.data.data.team_count,
            fansList: honey.mergeArray(that.data.fansList, res.data.data.list),
            pageNo: that.data.pageNo + 1,
            isLoad: (res.data.data.list.length < that.data.pageSize ? false : true)
          })
        }
      })
    }
  },
  fansCut:function(e){
      const that=this;
    const value = e.currentTarget.dataset.value;
      that.setData({
        fansSwitch:value,
        jibie:value,
        pageNo:1,
        isLoad: true,
        fansList:[]
      })
    that.getFansList();
      },
  navigateTo:function(){
    wx.navigateTo({
      url: '/pages/mine/myAmbassador',
    })
  }
})