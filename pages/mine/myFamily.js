import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {},
    pageNo: 1,
    pageSize: 10,
    isLoad: true,
    familyList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', '/pages/mine/myFamily'),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myFamily', false);
    that.setData({
      pageNo: 1,
      pageSize: 10,
      isLoad: true,
      familyList: [],
      user: user
    });
    that.getFamilyList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mine/myFamily?check_code=${that.data.user.check_code}`,
    }
  },
  getFamilyList: function() {
    const that = this;
    if (that.data.isLoad) {
      that.setData({
        isLoad: false
      })
      honey.post({
        alias: 'myAppointmentUser',
        data: {
        },
        success: function(res) {
          that.setData({
            familyList: honey.mergeArray(that.data.familyList, res.data.data.list),
            pageNo: that.data.pageNo + 1,
            isLoad: (res.data.data.list.length < that.data.pageSize ? false : true)
          })
        }
      })
    }
  }
})