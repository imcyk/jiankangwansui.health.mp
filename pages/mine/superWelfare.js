// pages/mine/superWelfare.js
import Honey from '../../utils/honey';
import Event from '../../utils/event'
let WxParse = require('../../wxParse/wxParse.js');

const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    price: '',//超级福利价格
    content:'',//超级福利富文本
    id:'',//超级福利id
    user:'',//用户信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that=this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', `/pages/mine/superWelfare?id=${options.id}`),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
    let user=wx.getStorageSync('user')
    honey.post({
      alias: 'superGift',
      data: {
        gift_id: options.id,
      },
      success: function(res) {
        WxParse.wxParse('article', 'html', res.data.data.giftInfo.title, that, 0);
      that.setData({
        user,
        id: options.id,
        price: res.data.data.giftInfo.price,
        id: options.id,
        //  content: res.data.data.giftInfo.title.replace('<br />', '').replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin:0 auto;" ')
      })
        that.getShareInfo()
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: that.data.shareMessage.title,
      path: `/pages/mine/superWelfare?check_code=${that.data.user.check_code}&id=${that.data.id}`,
      imageUrl: that.data.shareMessage.image
    }
  },

  //前往首页
  toIndex: function() {
    wx.switchTab({
      url: '/pages/index/index',
    })
  },

  //立即购买
  tobuy: function() {
    const that=this;
    honey.post({
      alias:'buySuperGift',
      data:{
        gift_id:that.data.id
      },
      success:function(res){
        let payParam = res.data.data.payParam;
        wx.requestPayment({
          timeStamp: payParam.timeStamp,
          nonceStr: payParam.nonceStr,
          package: payParam.package,
          signType: payParam.signType,
          paySign: payParam.paySign,
          success: function () {
            wx.showToast({
              title: '购买成功',
              icon:'none'
            })
            setTimeout(() => {
              wx.navigateTo({
                url: '/pages/discounts/index',
              })
            }, 1500)
          },
          fail: function (res) {
            wx.showToast({
              title: '支付失败',
              image: '/res/images/icon-shibai.png'
            })
          }
        })
      }
        
    })
  },
  // 分享信息配置
  getShareInfo: function () {
    const that = this;
    honey.post({
      alias: 'getShareInfo',
      data: {
        object_id: that.data.id,
        type: 4
      },
      success: function (res) {
        that.setData({
          shareMessage: res.data.data
        })
      }
    })
  },

})