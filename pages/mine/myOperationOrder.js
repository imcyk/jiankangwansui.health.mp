import Honey from '../../utils/honey';
import Event from '../../utils/event'

const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: '',
    pageNo: 1,
    pageSize: 10,
    isLoad: true,
    orderList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myAmbassadorOrder', true);
    that.setData({
      user: user
    })
    that.getOperationOrder();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    const that = this;
    that.getOperationOrder();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  getOperationOrder: function() {
    const that = this;
    if (that.data.isLoad) {
      that.setData({
        isLoad: false
      })
      honey.post({
        alias: 'dongshiOrder',
        data: {
          page: that.data.pageNo,
          page_size: that.data.pageSize,
          status: '-1'
        },
        success: function(res) {
          that.setData({
            orderList: honey.mergeArray(that.data.orderList, res.data.data.list),
            pageNo: that.data.pageNo + 1,
            isLoad: (res.data.data.list.length < that.data.pageSize ? false : true)
          })
        }
      })
    }
  }
})