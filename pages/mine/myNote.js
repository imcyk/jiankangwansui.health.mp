import Honey from '../../utils/honey';
import Event from '../../utils/event'


const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: '',
    pageNo: 1,
    pageSize: 10,
    isLoad: true,
    noteList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', '/pages/mine/myNote'),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
    const user = honey.checkLogin('/pages/mine/myNote', true);
    that.setData({
      user: user
    })
    that.getNoteList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myNote', true);
    that.setData({
      user: user
    })
    // that.getNoteList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    // const that = this;
    // that.setData({
    //   isLoad: true,
    //   page_no: 1,
    //   noteList: [],
    // })
    // that.getNoteList()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    const that = this;
    if (that.data.isLoad) {
      that.setData({
        page_no: that.data.page_no + 1,
      })
      that.getNoteList()
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mine/myNote?check_code=${that.data.user.check_code}`,
    }
  },
  getNoteList: function() {
    const that = this;
    event.emit('refreshUserInfo', 'userInfo');
    if (that.data.isLoad) {
      that.setData({
        isLoad: false
      })
      honey.post({
        alias: 'userLetter',
        data: {
          page: that.data.pageNo,
          page_size: that.data.pageSize,   
        },
        success: function(res) {
          that.setData({
            noteList: honey.mergeArray(that.data.noteList, res.data.data.list),
            pageNo: that.data.pageNo + 1,
            isLoad: (res.data.data.list.length < that.data.pageSize ? false : true)
          })
        }
      })
    }
  }
})