import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone:'',
    user:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/addRefereer', false);
    that.setData({
      user: user
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  inputPhone: function(e) {
    const that = this;
    that.setData({
      phone: e.detail.value
    })
  },
  saveReferee:function(){
    const that = this;
    const phone = that.data.phone;

    const phoneCheck = honey.matchRegular('phone', phone);
    if (!phoneCheck.result) {
      honey.showMiniToast({
        title: phoneCheck.msg
      })
      return;
    }

    honey.post({
      alias: 'bindReferee',
      data: {
        mobile: phone,
      },
      success: function (res) {
        honey.alert({
            title:'添加推荐人成功',
            content: `已添加${phone}为您的推荐人`,
            success:function(){
              wx.redirectTo({
                url: '/pages/mine/myReferrer',
              })
            }
        })
      }
    })
  }
})