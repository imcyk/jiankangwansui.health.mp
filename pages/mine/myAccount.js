import Honey from '../../utils/honey';
import Event from '../../utils/event'


const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', '/pages/mine/myAccount'),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    const that = this;
    event.on('getUserInfo', 'myAccount', function(data) {
      console.log('myAccount');
      that.setData({
        user: data
      })
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myAccount', false);
    that.setData({
      user: user
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
    event.remove('getUserInfo', 'myAccount');
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mine/myAccount?check_code=${that.data.user.check_code}`,
    }
  },
  uploadFace: function() {
    const that = this;
    wx.chooseImage({
      success: function(res) {
        var tempFilePaths = res.tempFilePaths;
        const uploadTask = honey.upload({
          alias: 'uploadImg',
          filePath: tempFilePaths[0],
          name: 'image',
          success: function(res) {
            const avatar = res.data.name;
            honey.post({
              alias:'updateProfile',
              data:{
                avatar,
                sms_enable: that.data.user.sms_enable	
              },
              success:function(res){
                that.refreshUserInfo();
              }
            })   
          }
        })
        uploadTask.onProgressUpdate((res) => {
          wx.showLoading({
            title: `上传${res.progress}%`,
            mask: true
          })
          if (res.progress >= 100) {
            wx.hideLoading();
          }
        })
      },
    })
  },
  refreshUserInfo() {
    const that = this;
    event.emit('refreshUserInfo', 'userInfo');
  },
  changePhone: function() {
    const that = this;
    honey.showMiniToast({
      title: '暂不支持修改手机号'
    })
  },
  exit: function () {
    wx.removeStorageSync('user');
    wx.removeStorageSync('phone');
    wx.removeStorageSync('checkCode')
    wx.removeStorageSync('time')
    wx.switchTab({
      url: '/pages/index/index',
    })
  },
  switchClick:function(e){
    const that=this;
    if(e.detail.value){
      honey.post({
        alias: 'updateProfile',
        data:{
          sms_enable:1
        },
        success: function (res) {
          if (res.data.status == 1) {
           wx.showToast({
             title: '开启短信提醒',
             icon: "none"
           })
          }
        }
      })
    }else{
      honey.post({
        alias: 'updateProfile',
        data: {
          sms_enable: 0
        },
        success: function (res) {
          if (res.data.status == 1) {
            wx.showToast({
              title: '关闭短信提醒',
              icon:"none"
            })
          }
        }
      })
    }
  }
})