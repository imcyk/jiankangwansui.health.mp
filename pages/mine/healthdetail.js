// pages/mine/healthdetail.js
import Honey from '../../utils/honey';
import Event from '../../utils/event'


const app = getApp();
const honey = new Honey();
const event = new Event();


Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: "",
    data: "",
    content: '',
    healthId: "",
    bottomshow: false,
    imagehealthimage: false,
    money: '', //阅读领取健康基金值
    load: true,
    shareMessage: {}, //分享信息配置
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    const id = options.id
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', '/pages/mine/healthdetail?id=' + options.id),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
    that.setData({
      healthId: id,
      user: wx.getStorageSync('user')
    })

    honey.post({
      alias: 'getArticleInfo',
      data: {
        id: id,
      },
      success: function(res) {
        that.setData({
          data: res.data.data,
          content: res.data.data.content.replace('<br />', '').replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin:0 auto;" ')
        })
      }
    })

    that.getShareInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: that.data.shareMessage.title,
      path: `/pages/mine/healthdetail?check_code=${that.data.user.check_code}&id=${that.data.healthId}`,
      imageUrl: that.data.shareMessage.image
    }
  },
  healthArticlefund: function() {
    const that = this;
    if (that.data.load) {
      that.setData({
        bottomshow: true,
      })
    }
  },
  healthArticleFund: function(e) {
    const that = this;
    const formId = e.detail.formId;
    honey.post({
      alias: 'saveFormId',
      data: {
        form_id: formId,
      },
      success: function(res) {

      }
    })
    wx.showLoading({
      mask: true
    })
    honey.post({
      alias: 'readGiftHealth',
      data: {
        form_id: e.detail.formId,
        id: that.data.healthId,
      },
      error: function(res) {
        wx.hideLoading()
        that.setData({
          bottomshow: false,
        })
      },
      success: function(res) {
        wx.hideLoading()
        if (res.data.data.gift==0){
          that.setData({
            bottomshow: false,
            load: false
          })
          wx.showToast({
            title: '健康基金已达上限',
            icon:'none',
            duration:2500
          })
        }else{
          that.setData({
            bottomshow: false,
            money: res.data.data.gift,
            imagehealthimage: true,
            load: false
          })
        }
        // event.emit('refreshUserInfo', 'userInfo');
      }
    })
  },
  healthHide: function() {
    const that = this;
    that.setData({
      bottomshow: false,
      imagehealthimage: false,
    })
  },
  imagefalse: function() {
    this.setData({
      imagehealthimage: false
    })
  },

  //分享信息配置
  getShareInfo: function() {
    const that = this;
    honey.post({
      alias: 'getShareInfo',
      data: {
        object_id: that.data.healthId,
        type: 3
      },
      success: function(res) {
        that.setData({
          shareMessage: res.data.data
        })
      }
    })
  },
  saveFormId: function(e) {
    const that = this;
    const formId = e.detail.formId;
    honey.post({
      alias: 'saveFormId',
      data: {
        form_id: formId,
      },
      success: function(res) {

      }
    })
  }


})