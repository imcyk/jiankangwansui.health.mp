// pages/mine/newindex.js
import Honey from '../../utils/honey';
import Event from '../../utils/event'


const app = getApp();
const honey = new Honey();
const event = new Event();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    showContent: {
      general: true,
      generalize: false,
      operation: false,

    },
    user: {},
    wxCode: '',
    showWxCode: false
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code)
        wx.setStorageSync('pagesUrl', '/pages/mine/newindex'),
        wx.setStorageSync('isTab', true),
        wx.switchTab({
        url: '/pages/mine/newindex'
        })
    }
    wx.hideTabBar({})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    const that = this;
    event.on('getUserInfo', 'myAccount', function(data) {
      that.setData({
        user: data
      })
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/newindex', true);
    that.setData({
      user: user
    })
    event.emit('refreshUserInfo', 'userInfo');
    if(wx.getStorageSync('user')){
      honey.post({
        alias: 'userQrcode',
        success: function (res) {
          if (res.data.status == 1) {
            that.setData({
              wxCode: res.data.data.qrcode
            })
          }
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    this.setData({
      showWxCode: false
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mine/newindex?check_code=${that.data.user.check_code}`
    }
  },
  switchTab: function(e) {
    const flag = e.currentTarget.dataset.targetKey;
    const newshowContent = {
      general: false,
      generalize: false,
      operation: false,
    };

    newshowContent[flag] = true;
    this.setData({
      showContent: newshowContent,
    })
  },
  tishi: function() {
    honey.showMiniToast({
      title: '您不是运营，没有权限！'
    })
  },
  showWxCode: function() {
    this.setData({
      showWxCode: true
    })
  },
  hideWxCode: function() {
    this.setData({
      showWxCode: false
    })
  },
  downloadWxCode: function() {
    const that = this;
    wx.showLoading({
      title: '正在处理图片',
      mask: true,
      complete: () => {
        wx.getImageInfo({
          src: that.data.wxCode,
          success: function(ret) {
            var path = ret.path;
            wx.saveImageToPhotosAlbum({
              filePath: path,
              success(result) {
                wx.showToast({
                  title: '保存成功',
                })
              },
              fail: (e) => {
                wx.showToast({
                  title: '下载失败',
                  image: '/res/images/icon-shibai.png'
                })
              },
              complete: () => {
                wx.hideLoading()
              }
            })
          },
          fail: () => {
            wx.hideLoading()
          }
        })
      }
    })

  },
  
  //去登陆
  toLogin(){
    wx.reLaunch({
      url: '/pages/index/index?show=',
    })
  }
  // exit: function () {
  //   wx.removeStorageSync('user');
  //   wx.navigateTo({
  //     url: '/pages/login/login',
  //   })
  // }
})