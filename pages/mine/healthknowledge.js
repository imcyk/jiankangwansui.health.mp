// pages/mine/healthknowledge.js
import Honey from '../../utils/honey';
import Event from '../../utils/event'


const app = getApp();
const honey = new Honey();
const event = new Event();


Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: "",
    rulesShow: false,
    healthstatus: false,
    cate_id: 0,
    page_no: 1,
    page_size: "",
    cate_list: "",
    lists: "",
    headImage:"",
    text:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code)
        wx.setStorageSync('pagesUrl', '/pages/mine/healthknowledge')
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    const that = this;
    that.healthArticleList(); 
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    if (wx.getStorageSync('user')) {
      that.setData({
        user: wx.getStorageSync('user')
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mine/healthknowledge?check_code=${that.data.user.check_code}`
    }
  },
  showRules: function() {
    const that = this;
    that.setData({
      rulesShow: true,
      healthstatus: true
    })
  },
  healthHide: function() {
    const that = this;
    that.setData({
      rulesShow: false,
      healthstatus: false
    })
  },
  healthArticleList: function() {
    const that = this;
    honey.post({
      alias: 'getArticleList',
      data: {
        cate_id: that.data.cate_id,
        page: that.data.page_no,
        page_size: that.data.page_size || 100000,
      },
      success: function(res) {
        that.setData({
          cate_list: res.data.data.articleCate,
          lists: res.data.data.articleList,
          text: res.data.data.richList[0].content.replace('<br />', '').replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin:0 auto;" '),
          headImage: res.data.data.topBanner[0].imgurl
        })
      }
    })
  },
  cateClick: function(e) {
    const that = this;
    that.setData({
      cate_id: e.currentTarget.dataset.cateId,
      page_no: 1
    })
    that.healthArticleList();
  },
  healthDetail: function(e) {
    const that = this;
    wx.navigateTo({
      url: '/pages/mine/healthdetail?id=' + e.currentTarget.dataset.healthId,
    })
  },

  toIndex(){
    wx.switchTab({
      url: '/pages/index/index',
    })
  }
})