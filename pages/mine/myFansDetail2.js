// pages/mine/myFansDetail2.js

import Honey from '../../utils/honey';
import Event from '../../utils/event'

const app = getApp();
const honey = new Honey();
const event = new Event();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: "",
    searchId: "",
    fansDetail: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', '/pages/mine/myFansDetail2'),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
    const id = options.id;
    that.setData({
      searchId: id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this;
    if (wx.getStorageSync('user')) {
      that.setData({
        user: wx.getStorageSync('user')
      })
    }
    honey.post({
      alias: 'getChildInfo',
      data: {
        child_id: that.data.searchId,
      },
      success: function (res) {
        that.setData({
          fansDetail: res.data.data,
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mine/myFansDetail2?check_code=${that.data.user.check_code}`,
    }
  }
})