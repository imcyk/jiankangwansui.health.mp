import Honey from '../../utils/honey';
import Event from '../../utils/event'


const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {},
    note: '',
    content:"",
    withdraw:{}//提现数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    event.on('getUserInfo', 'myFansOrder', function(data) {
      that.setData({
        user: data
      })
    })

    honey.post({
      alias: 'userAmount',
      data: {
        minpage: '/' + that.route
      },
      success: function (res) {
        if (res.data.status == 1) {
          that.setData({
            content: res.data.data.richLists[0].content.replace('<br />', '').replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin:0 auto;" '),
            withdraw: res.data.data.withdraw
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myFansOrder', true);
    that.setData({
      user: user
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  inputNote: function(e) {
    const that = this;
    that.setData({
      note: e.detail.value
    })
  },
  toWidthdraw: function(e) {
    const flag = e.currentTarget.dataset.id;
    const that = this;
    const note = that.data.note.trim();
    if (note == '') {
      honey.showMiniToast({
        title: '请输入提现方式和账户'
      })
      return;
    }
    honey.post({
      alias: 'withdraw',
      data: {
        user_id: that.data.user.id,
        token: that.data.user.token,
        type: 2,
        amount: that.data.rechargeSetList[flag],
        note: note
      },
      success: function(res) {
        event.emit('refreshUserInfo', 'userInfo');
        honey.alert({
          title: '申请提取运营基金成功',
          content: '请留意站内信通知\n正在返回个人中心',
          success: function() {
            wx.switchTab({
              url: '/pages/mine/index',
            })
          }
        })
      }
    })
  }
})