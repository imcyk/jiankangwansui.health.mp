import Honey from '../../utils/honey';
import Event from '../../utils/event'

const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {},
    rechargeSetList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', '/pages/mine/myBalance'),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
    event.on("getUserInfo", "myBalance", function(data) {
      that.setData({
        user: data
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    const that = this;
    // honey.post({
    //   alias: 'rechargeSet',
    //   success: function(res) {
    //     that.setData({
    //       rechargeSetList: res.data.data
    //     })
    //   }
    // })

    honey.post({
      alias: 'userAmount',
      data: {
        minpage: '/' + that.route
      },
      success: function(res) {
        if (res.data.status == 1) {
          that.setData({
            content: res.data.data.richLists[0].content.replace('<br />', '').replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin:0 auto;" '),
            rechargeSetList: res.data.data.recharge
          })
        }
      }
    })

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myBalance', false);
    that.setData({
      user: user
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
    event.remove("getUserInfo", "myBalance")
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mine/myBalance?check_code=${that.data.user.check_code}`,
    }
  },
  rechargeByMoney: function(e) {
    var that = this;
    const money = e.currentTarget.dataset.money;
    wx.showLoading({
      title: '支付中',
    })
    honey.post({
      alias: 'recharge',
      data: {
        money: money
      },
      success: function(res) {
        const that = this;
        var payParam = res.data.data.payParam;
        console.log(payParam)
        wx.requestPayment({
          timeStamp: payParam.timeStamp,
          nonceStr: payParam.nonceStr,
          package: payParam.package,
          signType: payParam.signType,
          paySign: payParam.paySign,
          success: function() {
            event.emit('refreshUserInfo', 'userInfo');
            // wx.switchTab({
            //   url: '/pages/mine/newindex',
            // })
          },
          fail: function(res) {
            wx.showToast({
              title: '支付失败',
              icon: 'none'
            })
            wx.switchTab({
              url: '/pages/mine/index',
            })
          }
        })
      },
      complete: function() {
        wx.hideLoading();
      }
    })




  }
})