import Honey from '../../utils/honey';
import Event from '../../utils/event'


const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {},
    note: '',
    content:'',
    withdraw:{},//提现数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', '/pages/mine/myGeneralizeFoundation'),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
    event.on('getUserInfo', 'myGeneralizeFoundation', function(data) {
      that.setData({
        user: data
      })
    })

    honey.post({
      alias: 'userAmount',
      data: {
        minpage: '/' + that.route
      },
      success: function (res) {
        if (res.data.status == 1) {
          that.setData({
            content: res.data.data.richLists[0].content.replace('<br />', '').replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin:0 auto;" '),
            withdraw: res.data.data.withdraw
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myGeneralizeFoundation', true);
    that.setData({
      user: user
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mine/myGeneralizeFoundation?check_code=${that.data.user.check_code}`,
    }
  },
  inputNote: function(e) {
    const that = this;
    that.setData({
      note: e.detail.value
    })
  },
  toWidthdraw: function(e) {
    const flag = e.currentTarget.dataset.id;
    const that = this;
    const note = that.data.note.trim();
    if (note == '') {
      honey.showMiniToast({
        title: '请按以下格式输入：1.开户行、2.账号、3.户名；也可通过填写微信，支付宝账号进行收款'
      })
      return;
    }
    honey.post({
      alias: 'doWithdraw',
      data: {
        type: 'fenxiao',
        money: that.data.withdraw.values[flag],
        smeta: note
      },
      success: function(res) {
        event.emit('refreshUserInfo', 'userInfo');
        honey.alert({
          title: '申请提取推广基金成功',
          content: '请留意站内信通知\n正在返回个人中心',
          success: function() {
            wx.switchTab({
              url: '/pages/mine/index',
            })
          }
        })
      }
    })
  }
})