// pages/mine/myFansDetail1.js

import Honey from '../../utils/honey';
import Event from '../../utils/event'

const app = getApp();
const honey = new Honey();
const event = new Event();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    user:"",
    pageNo:1,
    pageSize:10,
    isLoad:true,
    searchId:"",
    fansList:{},
    fansDetail:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     const that=this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', '/pages/mine/myFansDetail1'),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
    const id = options.id;
    that.setData({
      searchId:id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
     const that=this;
     if(wx.getStorageSync('user')){
          that.setData({
            user: wx.getStorageSync('user')
        })
     }
    that.getFansList();
    honey.post({
      alias: 'getChildInfo',
      data: {
        child_id: that.data.searchId,
      },
      success: function (res) {
        that.setData({
          fansDetail: res.data.data,
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    const that = this;
    that.getFansList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mine/myFansDetail1?check_code=${that.data.user.check_code}`,
    }
  },
  getFansList: function () {
    const that = this;
    if (that.data.isLoad) {
      that.setData({
        isLoad: false
      })
      honey.post({
        alias: 'getChildList',
        data: {
          page: that.data.pageNo,
          page_size: that.data.pageSize,
          cate:'team',
          user_id: that.data.searchId,
          type:'first'
        },
        success: function (res) {
          that.setData({
            fansList: honey.mergeArray(that.data.fansList, res.data.data.list),
            pageNo: that.data.pageNo + 1,
            isLoad: (res.data.data.list.length < that.data.pageSize ? false : true)
          })
        }
      })
    }
  },
})