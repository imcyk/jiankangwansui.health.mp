import Honey from '../../utils/honey';
import Event from '../../utils/event'

const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: '',
    data:{},//升级显示数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myDongshidongshi_infodongshi_infodongshi_infodongshiUpdate', false);
    that.setData({
      user: user
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this;
    honey.post({
      alias: 'toUpgrade',
      data: {
        type: 'dongshi'
      },
      success: function (res) {
        that.setData({
          data: res.data.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  buyUpdate:function(){
    wx.navigateTo({
      url: '/pages/mine/applyForOperation',
    })
  }
})