import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    user: [],
    family: {
      name: '',
      phone: '',
      relation: '',
      relation_name: '',
      sex:1,
    },
    sex:[
      { name: '男', value: 1,checked: 'true'},
      { name: '女', value: 2}
      ],
    relation: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', '/pages/mine/myFamilyEdit'),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
    const id = options.id ? options.id : '';
    that.setData({
      id,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    const that = this;
    honey.post({
      alias: 'myAppointmentUser',
      success: function(res) {
        that.setData({
          relation: res.data.data.relation_arr
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const id = that.data.id;
    const user = honey.checkLogin('/pages/mine/myFamily', false);
    that.setData({
      user: user
    });

    if (id) {
      honey.post({
        alias: 'myAppointmentUser',
        success: function(res) {
          let family={};
          let list = res.data.data.list;
          for(let i=0;i<list.length;i++){
            if (list[i].id==id){
              family.name = list[i].family_name;
              family.phone = list[i].family_phone;
              family.relation = list[i].family_relation;
              family.relation_name = list[i].relation_name;
              family.sex=list[i].sex;
              let sex=that.data.sex;
              sex[0].checked = family.sex == 1 ? true : false;
              sex[1].checked = family.sex == 2 ? true : false;
              that.setData({
                sex,
              })
              break;
            }
          }
          // family.relation_id = family.relation;
          // delete family.relation
          that.setData({
            family,
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mine/myFamilyEdit?check_code=${that.data.user.check_code}`,
    }
  },
  inputInfo: function(e) {
    const that = this;
    const key = e.currentTarget.dataset.key;
    const value = e.detail.value;
    let family = that.data.family;
    family[key] = value;
    that.setData({
      family: family
    })
  },
  changeRelation: function(e) {
    const that = this;
    const relation = that.data.relation;
    const pickerIndex = e.detail.value;
    let selectRelation = relation[pickerIndex];
    selectRelation.relation = selectRelation.relation_id;
    let family = that.data.family;
    family = Object.assign(family, selectRelation);
    that.setData({
      family,
    })
  },
  checkFamily: function() {
    const that = this;
    let family = new Object();
    family.id=that.data.id;
    family.name = that.data.family.name.trim();
    family.phone = that.data.family.phone;
    // family.family_id = that.data.family.id;
    family.relation = that.data.family.relation;
    family.sex = that.data.family.sex;

    if (family.name == '') {
      honey.showMiniToast({
        title: '请输入家人姓名'
      })
      return false;
    }
    if (family.relation == '') {
      honey.showMiniToast({
        title: '请选择家人关系'
      })
      return false;
    }

    const phoneCheck = honey.matchRegular('phone', family.phone);
    if (!phoneCheck.result) {
      honey.showMiniToast({
        title: phoneCheck.msg
      })
      return false;
    }

    return family;
  },
  updataFamily: function() {
    const that = this;
    const family = that.checkFamily();

    if (family) {
      const user = that.data.user;
      honey.post({
        alias: 'updAppointmentUser',
        data: family,
        success: function(res) {
          if (res.data.status == 1) {
            honey.showMiniToast({
              title: '修改成功\n正在返回上一页',
              success: function() {
                setTimeout(() => {
                  wx.navigateBack()
                }, 1500)
              }
            })
          }
        }
      })
    }
  },
  addFamily: function() {
    const that = this;
    const family = that.checkFamily();
    if (family) {
      const user = that.data.user;
      honey.post({
        alias: 'updAppointmentUser',
        data: family,
        success: function (res) {
          if (res.data.status == 1) {
            honey.showMiniToast({
              title: '保存成功\n正在返回上一页',
              success: function () {
                setTimeout(() => {
                  wx.navigateBack()
                }, 1500)
              }
            })
          }
        }
      })
    }
  },
  delFamily: function() {
    const that = this;
    honey.confirm({
      title: '确认删除？',
      content: '删除家人将不可以恢复，是否删除',
      confirmText: '删除',
      success: function(res) {
        if (res.confirm) {
          honey.post({
            alias: 'delAppointmentUser',
            data: {
              id:that.data.id
            },
            success: function(res) {
              if (res.data.status == 1) {
                honey.showMiniToast({
                  title: '删除成功\n正在返回上一页',
                  success: function() {
                    setTimeout(() => {
                      wx.redirectTo({
                        url: '/pages/mine/myFamily',
                      })
                    }, 1500)
                  }
                })
              }
            }
          })
        }
      }
    })
  },
  //性别选择
  radioChange: function (e) {
    const that=this;
    let family = that.data.family;
    family.sex = e.detail.value;
    that.setData({
      family,
    })
  }
})