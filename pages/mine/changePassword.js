import Honey from '../../utils/honey';
import Event from '../../utils/event'


const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {},
    form: {
      phone: '',
      code: '',
      password: '',
      checkPw: ''
    },
    parentId: '',
    sendBtnText: '发送验证码',
    canSendCode: true

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/index', true);
    let form = that.data.form;
    form.phone = user.phone;
    that.setData({
      user: user,
      form: form
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  inputValue: function(e) {
    const that = this;
    const key = e.currentTarget.dataset.key
    const value = e.detail.value;
    let form = that.data.form;
    form[key] = value;
    that.setData({
      form: form
    })
  },
  toChange: function() {
    const that = this;
    const phone = that.data.form.phone;
    const code = that.data.form.code.trim();
    const password = that.data.form.password;
    const checkPw = that.data.form.checkPw;
    const parentId = that.data.form.parentId;

    const phoneCheck = honey.matchRegular('phone', phone);
    if (!phoneCheck.result) {
      honey.showMiniToast({
        title: phoneCheck.msg
      })
      return;
    }

    const codeCheck = honey.matchRegular('code', code);
    if (!codeCheck.result) {
      honey.showMiniToast({
        title: codeCheck.msg
      })
    }

    const codePassword = honey.matchRegular('password', password);
    if (!codePassword.result) {
      honey.showMiniToast({
        title: password.msg
      })
    }

    if (password != checkPw) {
      honey.showMiniToast({
        title: '登录密码和确认密码不一致\n请确认后输入'
      })
    }

    honey.post({
      alias: 'forgetPassword',
      data: {
        phone: phone,
        code: code,
        password: password,
      },
      success: function(res) {
        honey.alert({
          title: '修改成功',
          content: '您已经修改了密码，正在返回个人中心！',
          success: function(res) {
            if (res.confirm) {
              wx.setStorageSync('phone', phone);
              wx.setStorageSync('password', password);
              wx.switchTab({
                url: '/pages/mine/index',
              })
            }
          }
        })
      }
    })

  },
  sendCode: function() {
    const that = this;
    const phone = that.data.form.phone;

    const conclusion = honey.matchRegular('phone', phone);
    if (!conclusion.result) {
      honey.showMiniToast({
        title: conclusion.msg
      })
      return;
    }
    honey.post({
      alias: 'sendCode',
      data: {
        phone: phone,
        type: 'password'
      },
      success: function(res) {
        that.toSendCode();
      }
    })
  },
  toSendCode: function() {
    const that = this;
    let sendAgainTime = 60;
    that.setData({
      sendBtnText: '60秒重发',
      canSendCode: false
    })
    let sendTimer = setInterval(() => {
      sendAgainTime--;
      if (sendAgainTime == 0) {
        clearInterval(sendTimer)
        that.setData({
          sendBtnText: '发送验证码',
          canSendCode: true
        })
      } else {
        that.setData({
          sendBtnText: `${sendAgainTime}后重发`,
          canSendCode: false
        })
      }
    }, 1000)
  }
})