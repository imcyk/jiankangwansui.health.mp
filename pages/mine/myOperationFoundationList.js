import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {},
    pageNo: 1,
    pageSize: 10,
    isLoad: true,
    logList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myGeneralizeFoundationList', false);
    that.setData({
      user: user
    });
    that.getLog()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    const that = this;
    that.getFundLog();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getLog: function () {
    const that = this;
    if (that.data.isLoad) {
      that.setData({
        isLoad: false
      })
      honey.post({
        alias: 'amountLog',
        data: {
          page: that.data.pageNo,
          page_size: that.data.pageSize,
          type: 'dongshi_amount'
        },
        success: function (res) {
          that.setData({
            logList: honey.mergeArray(that.data.logList, res.data.data.data),
            pageNo: that.data.pageNo + 1,
            isLoad: (res.data.data.data.length < that.data.pageSize ? false : true)
          })
        }
      })
    }

  }
})