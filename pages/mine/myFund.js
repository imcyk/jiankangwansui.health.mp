import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {},
    pageNo: 1,
    pageSize: 10,
    isLoad: true,
    logList: [],
    content:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', '/pages/mine/myFund'),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
    honey.post({
      alias: 'userAmount',
      data: {
        minpage: '/'+that.route
      },
      success: function(res) {
        if (res.data.status == 1) {
          that.setData({
            content: res.data.data.richLists[0].content.replace('<br />', '').replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin:0 auto;" ')
          })
        }
      }
    })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myFund', false);
    that.setData({
      user: user
    });
    // that.getRenewLog()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    const that = this;
    // that.getRenewLog();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mine/myFund?check_code=${that.data.user.check_code}`,
    }
  },
  getRenewLog: function() {
    const that = this;
    if (that.data.isLoad) {
      that.setData({
        isLoad: false
      })
      honey.post({
        alias: 'renewLog',
        data: {
          page_no: that.data.pageNo,
          page_size: that.data.pageSize,
          is_jk: 1,
          user_id: that.data.user.id,
          token: that.data.user.token
        },
        success: function(res) {
          that.setData({
            logList: honey.mergeArray(that.data.logList, res.data.data),
            pageNo: that.data.pageNo + 1,
            isLoad: (res.data.data.length < that.data.pageSize ? false : true)
          })
        }
      })
    }

  }
})