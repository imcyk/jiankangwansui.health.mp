import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    form: {
      phone: '',
      code: '',
      // password: '',
      // checkPw: '',
      // check_code: ''
    },
    parentId: '',
    sendBtnText: '发送验证码',
    canSendCode: true,
    idcard:'',//身份证
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    console.log(options.code)
    let form = that.data.form;
    form.check_code = options.code
    if (options.code) {
      that.setData({
        form: form
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  inputValue: function(e) {
    const that = this;
    const key = e.currentTarget.dataset.key
    const value = e.detail.value;
    let form = that.data.form;
    form[key] = value;
    that.setData({
      form: form
    })
  },
  toRegister: function() {
    const that = this;
    const phone = that.data.form.phone;
    const code = that.data.form.code.trim();
    const password = that.data.form.password;
    const checkPw = that.data.form.checkPw;
    const parentId = that.data.form.parentId;

    const phoneCheck = honey.matchRegular('phone', phone);

    if (!phoneCheck.result) {
      honey.showMiniToast({
        title: phoneCheck.msg
      })
      return;
    }

    const codeCheck = honey.matchRegular('code', code);
    if (!codeCheck.result) {
      honey.showMiniToast({
        title: codeCheck.msg
      })
    }

    // const codePassword = honey.matchRegular('password', password);
    // if (!codePassword.result) {
    //   honey.showMiniToast({
    //     title: password.msg
    //   })
    // }

    // if (password != checkPw) {
    //   honey.showMiniToast({
    //     title: '登录密码和确认密码不一致\n请确认后输入'
    //   })
    // }

    if(that.data.idcard.length!=18){
      honey.showMiniToast({
        title: '身份证号码错误'
      })
      return;
    }

    wx.showLoading({
      title: "加载中"
    });
    honey.post({
      alias: 'checkSms',
      data: {
        mobile: phone,
        captcha: code,
        event: 'default'
      },
      success: function(res) {
        wx.hideLoading();
        wx.navigateTo({
          url: '/pages/report/index?phone=' + phone+'&code='+code+'&idcard='+that.data.idcard,
        })
        // honey.alert({
        //   title: '恭喜您',
        //   content: '您已经是我们的会员了，返回登录',
        //   success: function(res) {
        //     if (res.confirm) {
        //       wx.setStorageSync('phone', phone);
        //       wx.setStorageSync('password', password);
        //       wx.redirectTo({
        //         url: '/pages/login/login',
        //       })
        //     }
        //   }
        // })
      }
    })

  },
  sendCode: function() {
    const that = this;
    const phone = that.data.form.phone;

    const conclusion = honey.matchRegular('phone', phone);
    if (!conclusion.result) {
      honey.showMiniToast({
        title: conclusion.msg
      })
      return;
    }
    honey.post({
      alias: 'sendSms',
      data: {
        mobile: phone,
        event: 'default'
      },
      success: function(res) {
        that.toSendCode();
      }
    })
  },
  toSendCode: function() {
    const that = this;
    let sendAgainTime = 60;
    that.setData({
      sendBtnText: '60秒重发',
      canSendCode: false
    })
    let sendTimer = setInterval(() => {
      sendAgainTime--;
      if (sendAgainTime == 0) {
        clearInterval(sendTimer)
        that.setData({
          sendBtnText: '发送验证码',
          canSendCode: true
        })
      } else {
        that.setData({
          sendBtnText: `${sendAgainTime}后重发`,
          canSendCode: false
        })
      }
    }, 1000)
  },

  inputIdcard(e){
  const that=this;
  that.setData({
    idcard:e.detail.value,
  })
  }
})