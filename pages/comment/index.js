import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderId: {},
    user: {},
    order: {},
    comment: '',
    imgArr: [],//图片数组
    videoArr:[],//视频数组
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    const orderId = options.id;
    that.setData({
      orderId: orderId,
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/subscribe/index?id=' + that.data.orderId, false);
    that.setData({
      user: user
    })
    honey.post({
      alias: 'orderInfo',
      data: {
        order_sn: that.data.orderId
      },
      success: function(res) {
        that.setData({
          order: res.data.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  inputComment: function(e) {
    const that = this;
    that.setData({
      comment: e.detail.value
    })
  },
  saveComment: function() {
    const that = this;
    const user = that.data.user;
    const comment = that.data.comment.trim();
    if (comment == '') {
      honey.showMiniToast({
        title: '请写点评论吧～'
      })
      return false;
    }
    honey.post({
      alias: 'commentOrder',
      data: {
        order_sn: that.data.orderId,
        content: comment,
        image: JSON.stringify(that.data.imgArr),
        video:JSON.stringify(that.data.videoArr)
      },
      success: function(res) {
        honey.showMiniToast({
          title: '评论成功'
        })
        setTimeout(() => {
          wx.navigateBack()
        }, 1000);
      }
    })
  },

  //添加图片
  addCamera:function(){
      const that=this;
    wx.chooseImage({
      success(res) {
        const tempFilePaths = res.tempFilePaths
        if ((tempFilePaths.length+that.data.imgArr.length)>8){
           wx.showToast({
             title: '图片总数大于8张',
             icon:'none'
           })
        }else{
          wx.showLoading({
            title: '上传中...',
          })
          for (let i=0; i < tempFilePaths.length; i++) {
            honey.upload({
              alias: 'uploadImg',
              filePath: tempFilePaths[i],
              name: 'image',
              success(res) {
                let imgArr = that.data.imgArr.concat(res.data.name)
                that.setData({
                  imgArr,
                })
              },
              complete:function(){
                wx.hideLoading();
              }
            })
          }
        }
      },
    })
  },

  //删除图片
  delimage:function(e){
   const that=this;
   const index = e.currentTarget.dataset.index;
    let data =that.data.imgArr;
    data.splice(index,1);
   that.setData({
     imgArr:data,
   })
    
  },

  //上传视频
  addvedio:function(){
    
    const that=this;
    wx.chooseVideo({
      sourceType: ['album', 'camera'],
      maxDuration: 60,
      camera: 'back',
      success(res) {
        wx.showLoading({
          title: '上传中...',
        })
        honey.upload({
          alias:'uploadVideo',
          filePath: res.tempFilePath,
          name:'video',
          success:function(res){
            let videoArr = that.data.videoArr.concat(res.data.name)
            that.setData({
              videoArr
              })
          },
          complete:function(){
           wx.hideLoading();
          }
        })
      }
    })
  },

  //删除视频
  delvideo: function (e) {
    const that = this;
    that.setData({
      videoArr: [],
    })

  },

})