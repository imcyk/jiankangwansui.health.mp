import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {},
    pageNo: 1,
    pageSize: 10,
    isLoad: true,
    commentList: [],
    goodid: '',//商品ID
    storeid:''//商品ID
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that=this;
    if (options.goodid){
       that.setData({
         goodid: options.goodid
       })
    }else{
      that.setData({
        storeid: options.storeid
      })
    }
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', `/pages/comment/list?goodid=${goodid}&&storeid=${storeid}`),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this;
    const user = honey.checkLogin('/pages/comment/list', false);
    that.setData({
      user: user
    });
    that.getCommentList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    const that = this;
    that.getCommentList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/comment/list?check_code=${that.data.user.check_code}&&goodid=${goodid}&&storeid=${storeid}`,
    }
  },

  //获取评价列表数据
  getCommentList: function () {
    const that = this;
    if (that.data.isLoad) {
      that.setData({
        isLoad: false
      })
      honey.post({
        alias: 'commentList',
        data: {
          page: that.data.pageNo,
          page_size: that.data.pageSize,
          goods_id: that.data.goodid,
          store_id: that.data.storeid
        },
        success: function (res) {
          that.setData({
            commentList: honey.mergeArray(that.data.commentList, res.data.data.comment_list),
            pageNo: that.data.pageNo + 1,
            isLoad: (res.data.data.comment_list.length < that.data.pageSize ? false : true)
          })
        }
      })
    }

  },

  //点击打开图片
  previewImage: function (e) {
    const that = this;
    const image = e.currentTarget.dataset.image;
    console.log(image);
    const index = e.currentTarget.dataset.index;
    console.log(index);
    wx.previewImage({
      current: image, // 当前显示图片的http链接
      urls: that.data.commentList[index].smeta.image // 需要预览的图片http链接列表
    })
  }
})