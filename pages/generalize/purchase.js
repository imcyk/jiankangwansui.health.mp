import Honey from '../../utils/honey';
import Event from '../../utils/event'

const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user: {},
    content: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code);
    }
    event.on('getUserInfo', 'generalizePurchase', function(data) {
      that.setData({
        user: data
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    const that = this;
    honey.post({
      alias: 'upgradePage',
      data: {
        type: 'fenxiao',
        minpage: '/' + that.route
      },
      success: function(res) {
        let content = res.data.data;
        content.rich_text[0].content = content.rich_text[0].content.replace('<br />', '').replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin:0 auto;" ');
        that.setData({
          content,
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    const user = honey.checkLogin('/pages/generalize/purchase', false);
    that.setData({
      user: user
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
    event.remove('getUserInfo', 'generalizePurchase')
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/generalize/purchase?check_code=${wx.getStorageSync('user').check_code}`,
    }
  },
  buyUpdateBtn: function() {
    var that = this;
    wx.showLoading({
      title: '支付中',
    })
    honey.post({
      alias: 'updateGrade',
      data: {
        money: that.data.content.level_buy,
        type: 'fenxiao'
      },
      success: function(res) {
        wx.hideLoading();
        var payParam = res.data.data.payParam;
        wx.requestPayment({
          timeStamp: payParam.timeStamp,
          nonceStr: payParam.nonceStr,
          package: payParam.package,
          signType: payParam.signType,
          paySign: payParam.paySign,
          success: function() {
            event.emit('refreshUserInfo', 'userInfo');
            wx.showToast({
              title: '支付成功，返回个人中心',
            })
            setTimeout(() => {
              wx.switchTab({
                url: '/pages/mine/index',
              })
            }, 1500)
          },
          fail: function(res) {
            wx.hideLoading();
            wx.showToast({
              title: '支付失败',
              image: '/res/images/icon-shibai.png'
            })
            // wx.switchTab({
            //   url: '/pages/mine/newindex',
            // })
          },
        })
      },
    })
  }
})