import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    store: {},
    user:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', `/pages/organization/index?id=${options.id}`),
        wx.setStorageSync('isTab', false),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
    const that = this;
    const storeId = options.id;
    const imgReg = /<img.*?(?:>|\/>)/gi;
    const srcReg = /src=[\'\"]?([^\'\"]*)[\'\"]?/i;
    wx.getLocation({
      type: 'gcj02',
      complete: function (res) {
        that.setData({
          longitude: res.longitude ? res.longitude : '',
          latitude: res.latitude ? res.latitude : '',
          user: wx.getStorageSync('user')
        })
        honey.post({
          alias: 'storeInfo',
          data: {
            store_id: storeId,
            lat: res.latitude,
            lng: res.longitude
          },
          success: function (res) {
            let store = res.data.data;
            store.content = store.content.replace('<br />', '').replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin:0 auto;" ');
            var arr = store.content.match(imgReg);
            var imgArr = [];
            for (var i = 0; i < arr.length; i++) {
              var src = arr[i].match(srcReg);
              //获取图片地址
              if (src[1]) {
                imgArr.push(src[1]);
              }
            }
            that.setData({
              storeId: storeId,
              store: store,
              imgArr: imgArr
            })
          }
        })
      },
    })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    console.log(`/pages/organization/index?id=${that.data.storeId}&check_code=${that.data.user.check_code}`)
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/organization/index?id=${that.data.storeId}&check_code=${that.data.user.check_code}`
    }
  },
  prevImage:function(){
    const that = this;
    wx.previewImage({
      current: that.data.imgArr[0], // 当前显示图片的http链接
      urls: that.data.imgArr // 需要预览的图片http链接列表
    })
  }
})