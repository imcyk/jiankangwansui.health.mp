// pages/searchList/index.js
import Honey from '../../utils/honey';
import Event from '../../utils/event'

const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hotKey:[],//搜索热词数组
    categoryList:[],//常用分类数组
    keyword:'',//当前表单内容
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code)
        wx.setStorageSync('pagesUrl', '/pages/searchList/index')
        wx.setStorageSync('isTab', true),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    const that = this;
    honey.post({
      alias: 'getShopCate',
      success: function (res) {
        that.setData({
          categoryList: honey.mergeArray(that.data.categoryList, res.data.data.cateList)
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that=this;
     honey.post({
       alias:'getSearchHot',
       success:function(res){
         that.setData({
           hotKey: res.data.data.hotKey
         })
       }
     })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/searchList/index?check_code=${that.data.user.check_code}`
    }
  },

  //搜索
  toSearch:function(){
  const that=this;
  wx.navigateTo({
    url: `/pages/mall/newindex?keyword=${that.data.keyword}`,
  })
  },

  //更新表单内容
  inputKeyword: function (e) {
    const that = this;
    that.setData({
      keyword: e.detail.value
    })
  },
})