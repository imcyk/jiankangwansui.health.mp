import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headImage: '',
    comboList: '',
    pageNo: 1,
    pageSize: 10,
    canLoad: true,
    filter: {
      normal: true,
      sale: false,
      price: false,
    },
    sort: '',
    sexText: '适用性别',
    sex: '',
    user:{},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code);
      wx.setStorageSync('pagesUrl', '/pages/combo/list')
        wx.switchTab({
          url: '/pages/index/index',
        })
        return;
    }
    const that = this;
    // honey.post({
    //   alias: 'articleList',
    //   data: {
    //     type: 7,
    //     article_id: 9
    //   },
    //   success: function(res) {
    //     that.setData({
    //       headImage: res.data.data[0]
    //     })
    //   }
    // })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    that.getComboList();
    that.setData({
      user:wx.getStorageSync('user'),
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    const that = this;
    that.getComboList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/combo/list?check_code=${that.data.user.check_code}`
    }
  },
  getComboList: function() {
    const that = this;
    if (that.data.canLoad) {
      that.setData({
        canLoad: false
      })
      honey.post({
        alias: 'getGoodsList',
        data: {
          page: that.data.pageNo,
          page_size: that.data.pageSize,
          cate_id: 1,
          sort: that.data.sort,
          sex: that.data.sex,
          is_shop:-1,
        },
        success: function(res) {
          that.setData({
            comboList: honey.mergeArray(that.data.comboList, res.data.data.goodsList),
            pageNo: that.data.pageNo + 1,
            canLoad: (res.data.data.goodsList.length < that.data.pageSize ? false : true),
            headImage: res.data.data.advTop[0]
          })
        },
        fail: function() {
          that.setData({
            canLoad: true
          })
        }
      })
    }
  },
  tapNormal: function() {
    const that = this;
    if (!that.data.filter.normal) {
      let filter = that.data.filter;
      for (var key in filter) {
        filter[key] = false;
      }
      filter.normal = true;
      that.setData({
        filter: filter,
        sort: '',
        pageNo: 1,
        canLoad: true,
        comboList: []
      })
      that.getComboList();
    }
  },
  tapSale: function(e) {
    const that = this;
    const sort = that.data.sort == 5 ? 2 : 5;

    let filter = that.data.filter;
    for (var key in filter) {
      filter[key] = false;
    }

    filter.sale = true;
    that.setData({
      filter: filter,
      sort: sort,
      pageNo: 1,
      canLoad: true,
      comboList: []
    })
    that.getComboList();
  },
  tapPrice: function() {
    const that = this;
    const sort = that.data.sort == 3 ? 4 : 3;

    let filter = that.data.filter;
    for (var key in filter) {
      filter[key] = false;
    }

    filter.price = true;
    that.setData({
      filter: filter,
      sort: sort,
      pageNo: 1,
      canLoad: true,
      comboList: []
    })
    that.getComboList();
  },
  tapSex: function() {
    const that = this;
    const sheetArray = ['全部', '适用男性', '适用女性']
    wx.showActionSheet({
      itemList: sheetArray,
      success: function(res) {
        const tapIndex = res.tapIndex;
        let sexText = '适用性别';
        let sex = '';
        console.log(res.tapIndex)
        if (tapIndex != 0) {
          sexText = sheetArray[tapIndex];
          sex = tapIndex;
        }
        that.setData({
          sexText: sexText,
          sex: sex,
          pageNo: 1,
          canLoad: true,
          comboList: []
        })
        that.getComboList();
      }
    })
  },
  //头图跳转
  toShow:function(e){
    app.toShow(e)
  }
})