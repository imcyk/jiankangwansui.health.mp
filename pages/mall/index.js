import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageNo: 1,
    pageSize: 10,
    comboList: [],
    categoryList: [{
      id: '',
      name: '全部'
    }],
    canLoad: true,
    catId: '',
    user: {},
    keyword: '',
    searchFocus:false,
    healthstatus:false,
    scollTop:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', '/pages/mall/index'),
        wx.setStorageSync('isTab', true),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
    wx.hideTabBar({})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    const that = this;
    honey.post({
      alias: 'getShopCate',
      success: function(res) {
        that.setData({
          categoryList: honey.mergeArray(that.data.categoryList, res.data.data.cateList)
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    that.getComboList();
    const user = wx.getStorageSync('user');
    that.setData({
      user: user
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    this.healthHide();
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mall/index?check_code=${that.data.user.check_code}`
    }
  },
  getComboList: function() {
    const that = this;
    if (that.data.canLoad) {
      that.setData({
        canLoad: false
      })
      honey.post({
        alias: 'getGoodsList',
        data: {
          page: that.data.pageNo,
          page_size: that.data.pageSize,
          cate_id: that.data.catId,
          sort:'',
          sex:'',
        },
        success: function(res) {
          that.setData({
            comboList: honey.mergeArray(that.data.comboList, res.data.data.goodsList),
            pageNo: that.data.pageNo + 1,
            canLoad: (res.data.data.goodsList.length < that.data.pageSize ? false : true)
          })
          if(that.data.pageNo ==2){
            that.setData({
              scollTop:45,
            })
          }
        },
        fail: function() {
          that.setData({
            canLoad: true
          })
        }
      })
    }
  },
  tapCategoty: function(e) {
    const that = this;
    const catId = e.currentTarget.dataset.catId;
    that.setData({
      canLoad: true,
      pageNo: 1,
      catId: catId,
      comboList: [],
    })
    that.getComboList()
  },
  inputKeyword: function(e) {
    const that = this;
    that.setData({
      keyword: e.detail.value
    })
  },
  toSearch: function() {
    const that = this;
    wx.navigateTo({
      url: '/pages/search/index?keyword=' + that.data.keyword,
    })
  },
  toChangeBtn: function () {
    const that = this;
    that.setData({
      searchFocus: true
    })
  },
  toRestoreBtn: function () {
    const that = this;
    that.setData({
      searchFocus: false
    })
  },
  healthShow:function(){
    const that=this;
    that.setData({
      healthstatus:true
    })
  },
  healthHide:function(){
    const that = this;
    that.setData({
      healthstatus: false
    })
  }
})