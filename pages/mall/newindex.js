// pages/mall/newindex.js
import Honey from '../../utils/honey';
import util from '../../utils/util.js';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categoryList: wx.getStorageSync('categoryList') || [], //商品分类
    user: {}, //用户信息
    canLoad: true, //是否还有下一页
    pageNo: 1, //当前页
    pageSize: 10, //每页记录数
    catId: '', //分类id
    keyword: '', //关键字
    topPicImageViewSuffix: util.imageViewToWidthFunc(300),
    classifyPicImageViewSuffix: util.imageViewToWidthFunc(80),
    loadmall:false,//加载文字显示
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code)
      wx.setStorageSync('pagesUrl', 'pages/mall/newindex?keyword='),
        wx.switchTab({
          url: '/pages/index/index'
        })
    }
    if (options.id) {
      setTimeout(()=>{
        that.setData({
          catId: options.id,
        });
        that.getComboList();
      },350)
    }
    if (options.keyword || options.keyword=='') {
      that.setData({
        keyword: options.keyword,
      });
      that.getComboList1();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    const that = this;
    honey.post({
      alias: 'getShopCate',
      success: function(res) {
        const categoryList = res.data.data.cateList;
        that.setData({
          categoryList: categoryList
        })
        wx.setStorageSync('categoryList', categoryList)
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    const that = this;
    that.setData({
      user: wx.getStorageSync('user')
    })
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/mall/newindex?check_code=${that.data.user.check_code}`
    }
  },

  //获取商品列表
  getComboList: function() {
    
    const that = this;
    if (that.data.canLoad) {
      that.setData({
        canLoad: false,
        loadmall:true
      })
      setTimeout(()=>{
        honey.post({
          alias: 'getGoodsList',
          data: {
            page: that.data.pageNo,
            page_size: that.data.pageSize,
            cate_id: that.data.catId,
            keyword: that.data.keyword,
            sort: '',
            sex: '',
            is_shop: 1,
          },
          success: function (res) {
            that.setData({
              comboList: honey.mergeArray(that.data.comboList, res.data.data.goodsList),
              pageNo: that.data.pageNo + 1,
              canLoad: (res.data.data.goodsList.length < that.data.pageSize ? false : true),
              loadmall: false
            })
            if (that.data.pageNo == 2) {
              that.setData({
                scollTop: 45,
              })
            }
          },
          fail: function () {
            that.setData({
              canLoad: true,
            })
          }
        })
      },500)
      
    }
  },

  //获取商品列表
  getComboList1: function () {

    const that = this;
    if (that.data.canLoad) {
      that.setData({
        canLoad: false,
      })
      honey.post({
        alias: 'getGoodsList',
        data: {
          page: that.data.pageNo,
          page_size: that.data.pageSize,
          cate_id: that.data.catId,
          keyword: that.data.keyword,
          sort: '',
          sex: '',
          is_shop: 1,
        },
        success: function (res) {
          that.setData({
            comboList: honey.mergeArray(that.data.comboList, res.data.data.goodsList),
            pageNo: that.data.pageNo + 1,
            canLoad: (res.data.data.goodsList.length < that.data.pageSize ? false : true),
          })
          if (that.data.pageNo == 2) {
            that.setData({
              scollTop: 45,
            })
          }
        },
        fail: function () {
          that.setData({
            canLoad: true,
          })
        }
      })
    }
  },

  //选择分类
  tapCategoty: function(e) {
    const that = this;
    const catId = e.currentTarget.dataset.catId;
    that.setData({
      canLoad: true,
      pageNo: 1,
      catId,
      comboList: [],
      keyword: '',
    })
    that.getComboList1()
  },
})