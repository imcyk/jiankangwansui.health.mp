import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headImage: "",
    user: {},
    reportList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code);
      wx.setStorageSync('pagesUrl', '/pages/report/index')
      wx.switchTab({
        url: '/pages/index/index',
      })
      return;
    }
    that.setData({
      user: wx.getStorageSync('user')
    })

    if (options.phone) {
      honey.post({
        alias: 'helpReport',
        data: {
          page: 1,
          page_size: 10000,
          mobile:options.phone,
          code:options.code,
          idcard:options.idcard
        },
        success: function (res) {
          that.setData({
            reportList: res.data.data.reportList,
            headImage: res.data.data.topBanner[0]
          })

        }
      })
    } else {
      honey.post({
        alias: 'userReport',
        data: {
          page: 1,
          page_size: 10000,
          idcard: options.idcard
        },
        success: function(res) {
          that.setData({
            reportList: res.data.data.reportList,
            headImage: res.data.data.topBanner[0]
          })
        }
      })
    }



  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    const that = this;

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/report/index?check_code=${that.data.user.check_code}`
    }
  },
  downpdf: function(e) {
    const url = e.currentTarget.dataset.down;
    wx.showLoading({
      title: "加载中"
    });
    wx.downloadFile({
      url: url,
      success: function(res) {
        const filePath = res.tempFilePath
        wx.openDocument({
          filePath: filePath,
          success: function(res) {
            wx.hideLoading();
          }
        })
      }
    })

  },

  //头图跳转
  toShow:function(e){
    app.toShow(e)
  }

})