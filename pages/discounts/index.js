// pages/discounts/index.js
import Honey from '../../utils/honey';
import Event from '../../utils/event'

const app = getApp();
const honey = new Honey();
const event = new Event();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    couponList:[],//兑换券数据
    user:'',//用户信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that=this;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code),
        wx.setStorageSync('pagesUrl', '/pages/discounts/index'),
        wx.switchTab({
          url: '/pages/index/index'
        })
        return;
    }
    let user=wx.getStorageSync('user')
    that.setData({
      user,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that=this;
     honey.post({
       alias:'getCouponList',
       data:{
         goods_id:'',
         status:-1
       },
       success:function(res){
         that.setData({
           couponList: res.data.data.couponList,
         })
       }
     })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const that = this;
    return {
      title: `平台烧钱啦～点击领取大红包！`,
      path: `/pages/discounts/index?check_code=${that.data.user.check_code}`
    }
  },

  //前往搜索页
  toSearch:function(){
    console.log(111);
    wx.switchTab({
      url: '/pages/searchList/index',
    })
  }
    
})