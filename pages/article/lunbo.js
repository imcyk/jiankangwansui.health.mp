import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    actionInfo: {},
    id: '',
    addTime: '',
    shareMessage: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    const id = options.id;
    if (options.check_code && !wx.getStorageSync('user')) {
      wx.setStorageSync('checkCode', options.check_code);
      var pages = getCurrentPages() //获取加载的页面
      var currentPage = pages[pages.length - 1] //获取当前页面的对象
      var url = `/${currentPage.route}` //当前页面url
      var options = `${url}?id=${currentPage.options.id}` //如果要获取url中所带的参数可以查看options
      if (!wx.getStorageSync('user')) {
        wx.setStorageSync('pagesUrl', options)
        wx.switchTab({
          url: '/pages/index/index',
        })
      }
    }
    honey.post({
      alias: 'activityInfo',
      data: {
        id: id
      },
      success: function(res) {
        const actionInfo = res.data.data;
        actionInfo.content = actionInfo.content.replace('<br />', '').replace('style', 'data-code').replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin:0 auto;" ');
        that.setData({
          actionInfo: actionInfo,
          id,
        })
        that.getShareInfo();
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    const that = this;
      return {
        title: that.data.shareMessage.title,
        path: `/pages/article/lunbo?id=${that.data.id}&check_code=${wx.getStorageSync('user').check_code}`,
        imageUrl: that.data.shareMessage.image
      }
  },


  //分享信息配置
  getShareInfo: function() {
    const that = this;
    honey.post({
      alias: 'getShareInfo',
      data: {
        object_id: that.data.id,
        type: 2
      },
      success: function(res) {
        that.setData({
          shareMessage: res.data.data
        })
      }
    })
  },

  //返回首页
  toIndex: function() {
    wx.switchTab({
      url: '/pages/index/index',
    })
  }


})