import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bulletinInfo: {},
    id: '',
    addTime:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const that = this;
    const id = options.id;
    honey.post({
      alias: 'getNotice',
      data: {
        id: id
      },
      success: function(res) {
        const bulletinInfo = res.data.data;
        const addTime = bulletinInfo.create_time.replace('-', '年').replace('-', '月').replace(' ', '日 ');
        wx.setNavigationBarTitle({
          title: bulletinInfo.title,
        })
        bulletinInfo.content = bulletinInfo.content.replace('<br />', '').replace(/\<img/gi, '<img style="max-width:100%;height:auto;display:block;margin:0 auto;" ');
        that.setData({
          bulletinInfo: bulletinInfo,
          id: id,
          addTime
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})