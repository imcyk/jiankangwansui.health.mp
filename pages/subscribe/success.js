// pages/subscribe/success.js
import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info:{},
    img:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  const that=this;
  const id=options.id;
  that.setData({
    id,
  })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    const that=this;
    honey.post({
      alias: 'infoDatement',
      data: {
        id:that.data.id
      },
      success(result) {
        that.setData({
          info: result.data.data
        })
        wx.downloadFile({
          url: result.data.data.goods_thumb, //仅为示例，并非真实的资源
          success(res) {
            if (res.statusCode === 200) {
              that.setData({
                img: res.tempFilePath,
              })
              const info = result.data.data;
              const img = res.tempFilePath
              var ctx = wx.createCanvasContext('firstCanvas');
              ctx.setFillStyle('white');
              ctx.fillRect(0, 0, 300, 327);
              ctx.drawImage(img, 15, 15, 110, 79);
              ctx.font = 'normal bold 15px sans-serif';
              ctx.setFillStyle('black')
              if (info.store_name.length > 8) {
                let name = info.store_name.slice(0, 8) + '...';
                ctx.fillText(name, 140, 29);
              } else {
                ctx.fillText(info.store_name, 140, 29);
              }
              ctx.setFillStyle('#717171');
              ctx.font = 'normal 12px sans-serif';
              if (info.address.length > 12) {
                if (info.address.length <= 24) {
                  let address1 = info.address.slice(0, 12);
                  let address2 = info.address.slice(12);
                  ctx.fillText(address1, 140, 60);
                  ctx.fillText(address2, 140, 76);
                  ctx.fillText(info.shop_phone, 140, 92);
                } else {
                  let address1 = info.address.slice(0, 12);
                  let address2 = info.address.slice(12, 22);
                  let address3 = address2 + '...'
                  ctx.fillText(address1, 140, 60);
                  ctx.fillText(address3, 140, 76);
                  ctx.fillText(info.shop_phone, 140, 92);
                }
              }else{
                ctx.fillText(info.address, 140, 76);
                ctx.fillText(info.shop_phone, 140, 92);
              }
              ctx.setLineWidth(0.1);
              ctx.moveTo(0, 105);
              ctx.lineTo(300, 105);
              ctx.setFillStyle('black');
              ctx.font = 'normal bold 15px sans-serif';
              ctx.fillText('预约服务', 15, 130);
              ctx.moveTo(15, 142);
              ctx.lineTo(285, 142);
              ctx.fillText('预约门店', 15, 167);
              ctx.moveTo(15, 179);
              ctx.lineTo(285, 179);
              ctx.fillText('预约时间', 15, 204);
              ctx.moveTo(15, 216);
              ctx.lineTo(285, 216);
              ctx.fillText('预约人', 15, 241);
              ctx.moveTo(15, 253);
              ctx.lineTo(285, 253);
              ctx.fillText('预约人手机', 15, 278);
              ctx.moveTo(15, 290);
              ctx.lineTo(285, 290);
              ctx.fillText('订单号', 15, 315);
              ctx.font = 'normal 15px sans-serif';
              ctx.setFillStyle('#bbb');
              ctx.fillText('如需修改预约信息,请联系客服。', 15, 352);
              ctx.setFillStyle('#717171');
              ctx.setTextAlign('right');
              if (info.goods_name.length < 12) {
                ctx.fillText(info.goods_name, 285, 130);
              } else {
                let goodsname = info.goods_name.slice(0, 10);
                ctx.fillText(goodsname + '...', 285, 130);
              }
              ctx.fillText(info.shop_name, 285, 167);
              ctx.fillText(info.date + ' ' + info.time, 285, 204);
              ctx.fillText(info.family_name, 285, 241);
              ctx.fillText(info.family_phone, 285, 278);
              ctx.fillText(info.order_sn, 285, 315);
              ctx.clearRect(0, 0, 10, 10);
              ctx.clearRect(290, 0, 10, 10);
              ctx.clearRect(0, 317, 10, 10);
              ctx.clearRect(290, 317, 10, 10);
              ctx.stroke();
              ctx.draw();
            }
          }
        })
      }
    })

    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  //保存图片
  canvasSave:function(){
    wx.canvasToTempFilePath({
      canvasId: 'firstCanvas',
      success:function(res){
        var tempFilePath = res.tempFilePath;
        wx.saveImageToPhotosAlbum({
          filePath: tempFilePath,
          success(res) {
            wx.showToast({
              title: '保存成功',
            })
           }
        })
      },
      fail:function(res){

      }
    }, this)
  },
  
})