import Honey from '../../utils/honey';

const app = getApp();
const honey = new Honey();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headImage: '',
    user: '',
    orderList: [],
    isLoad: true,
    pageNo: 1,
    pageSize:10,
    goodList:{},
    storeId:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    const that = this;
    that.setData({
      storeId: options.storeId
    })
    honey.post({
      alias: 'articleList',
      data: {
        type: 7,
        article_id: 12
      },
      success: function (res) {
        that.setData({
          headImage: res.data.data[0],
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const that = this;
    const user = honey.checkLogin('/pages/mine/myPurchase', false);
    that.setData({
      user: user,
      orderList: [],
      isLoad: true,
      pageNo: 1
    })
    that.getOrderList();
    honey.post({
      alias: 'GetStoreCar',
      data: {
        store_id: that.data.storeId,
      },
      success: function (res) {
        that.setData({
          goodList: res.data.data,
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    const that = this;
    that.getOrderList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  getOrderList: function () {
    var that = this;
    if (that.data.isLoad) {
      that.setData({
        isLoad: false
      })
      honey.post({
        alias: 'orderList',
        data: {
          user_id: that.data.user.id,
          token: that.data.user.token,
          page_no: that.data.pageNo,
          status: 2,
          page_size: that.data.pageSize,
          store_id: that.data.storeId
        },
        success: function (res) {
          that.setData({
            orderList: honey.mergeArray(that.data.orderList, res.data.data),
            isLoad: (res.data.data.length < that.data.pageSize ? false : true),
            pageNo: that.data.pageNo + 1
          })
        }
      })
    }
  }
})